var iters      = [];
var data_delta = [];
var data_theta = [];
var data_alpha = [];
var data_beta  = [];
var data_gamma = [];
var data_eeg_6 = [];
var data_eeg_7 = [];
var data_eeg_8 = [];

if ( typeof fftjson === 'undefined' || fftjson == null )  {
	throw new Error("JSON data is not available!");
}

for(var i=0; i<fftjson["eeg"].length; i++) {
	data_delta.push( fftjson["eeg"][i][0] );
	data_theta.push( fftjson["eeg"][i][1] );
	data_alpha.push( fftjson["eeg"][i][2] );
	data_beta.push ( fftjson["eeg"][i][3] );
	data_gamma.push( fftjson["eeg"][i][4] );
	data_eeg_6.push( fftjson["eeg"][i][5] );
	data_eeg_7.push( fftjson["eeg"][i][6] );
	data_eeg_8.push( fftjson["eeg"][i][7] );
	iters.push(i);
}

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: iters,
		datasets: [
		{
			label: 'δ-ритм',
			data: data_delta,
			fill: false,
			backgroundColor: 'rgba(173, 173, 170, 0.8)',
			borderColor: 'rgba(173, 173, 170, 0.5)',
		},
		{
			label: 'θ-ритм',
			data: data_theta,
			fill: false,
			backgroundColor: 'rgba(54, 162, 235, 0.8)',
			borderColor: 'rgba(54, 162, 235, 0.5)',
		},
		{
			label: 'α-ритм',
			data: data_alpha,
			fill: false,
			backgroundColor: 'rgba(4, 207, 14, 0.8)',
			borderColor: 'rgba(4, 207, 14, 0.5)',
		},
		{
			label: 'β-ритм',
			data: data_beta,
			fill: false,
			backgroundColor: 'rgba(217, 22, 191, 0.8)',
			borderColor: 'rgba(217, 22, 191, 0.5)',
		},
		{
			label: 'γ-ритм',
			data: data_gamma,
			fill: false,
			backgroundColor: 'rgba(245, 220, 0, 0.8)',
			borderColor: 'rgba(245, 220, 0, 0.5)',
		},
		{
			label: 'ритм №5',
			data: data_eeg_6,
			fill: false,
			backgroundColor: 'rgba(57, 250, 192, 0.8)',
			borderColor: 'rgba(57, 250, 192, 0.5)',
			hidden: true,
		},
		{
			label: 'ритм №6',
			data: data_eeg_7,
			fill: false,
			backgroundColor: 'rgba(242, 174, 170, 0.8)',
			borderColor: 'rgba(242, 174, 170, 0.5)',
			hidden: true,
		},
		{
			label: 'ритм №7',
			data: data_eeg_8,
			fill: false,
			backgroundColor: 'rgba(171, 171, 245, 0.8)',
			borderColor: 'rgba(171, 171, 245, 0.5)',
			hidden: true,
		},
		],
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					min: -100
				},
				display: false
			}],
			xAxes: [{
			  ticks: { display: false }
			}],
		}
	}
});
