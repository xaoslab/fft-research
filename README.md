# Waveform fast Fourier transform → EEG

This is attempt to transpose music to brain rhythms (EEG).

## License

This is proprietary software. Please **carefully** read and understand [EULA](https://gitlab.com/xaoslab/fft-research/blob/master/LICENSE).

## Developer

Please contact via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## Contacts

[chaos lab.](https://xaoslab.tech)
